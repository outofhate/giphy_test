import org.jetbrains.kotlin.contracts.model.structure.UNKNOWN_COMPUTATION.type
import org.jetbrains.kotlin.js.translate.context.Namer.kotlin

buildscript {
    val kotlin_version: String by extra {
        "1.2.61"
    }

    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:3.2.0-rc02")
        classpath(kotlin("gradle-plugin", kotlin_version))
    }
}


allprojects {
    repositories {
        google()
        jcenter()
    }
}

task("clean") {
    delete(rootProject.buildDir)
}
