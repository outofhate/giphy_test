package com.outofdate.giphy

import android.annotation.SuppressLint
import android.app.Application
import com.outofdate.giphy.base.internal.ComponentsHandler
import com.outofdate.giphy.base.internal.DaggerAppComponent
import com.outofdate.giphy.base.internal.DaggerComponent
import com.outofdate.giphy.base.internal.android.AppModule
import java.util.*
import kotlin.reflect.KClass

@SuppressLint("Registered")
open class Giphy : Application(), ComponentsHandler {

    override fun onCreate() {
        super.onCreate()
        val component = DaggerAppComponent.builder().appModule(AppModule(this))
                .build()
        addComponent(component)
    }

    internal var components: MutableMap<String?, Any?> = HashMap()

    override fun addComponent(component: DaggerComponent?): DaggerComponent? {
        component?.javaClass?.simpleName?.replace("Dagger", "").run {
            components[this] = component
        }
        return component
    }

    override fun <C : DaggerComponent> hasComponent(component: KClass<C>): Boolean {
        return components.containsKey(component.simpleName)
    }

    override fun <C : DaggerComponent> getComponent(component: KClass<C>): C? = components[component.simpleName] as C?

    override fun <C : DaggerComponent> removeComponent(component: KClass<C>) {
        components.remove(component.simpleName)
    }

}
