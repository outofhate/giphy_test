package com.outofdate.giphy.modules.details

import android.net.Uri
import android.os.Build
import android.support.annotation.RequiresApi
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.request.ImageRequest
import com.outofdate.giphy.databinding.DetailsBinding
import com.outofdate.giphy.modules.giflist.model.VM
import com.outofdate.giphy.utils.Versions
import com.outofdate.giphy.utils.openUrl
import com.outofdate.giphy.utils.transitionName
import com.outofdate.giphy.utils.view.DefaultOutlineProvider

object GifDetailsBinder {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setupGif(binding: DetailsBinding?, vmIn: VM?) {
        vmIn?.let { v ->
            binding?.run {
                vm = v
                actionImage.run {
                    aspectRatio = v.original.ratio
                    transitionName(v.id)
                    setGifController(v.original.url, v.preview?.url)
                }
                val url = v.userInfo?.avatar
                avatar.setGifController(url, null)
                Versions.marshmallow {
                    constraint.outlineProvider = DefaultOutlineProvider.get()
                }
                profile.setOnClickListener {
                    v.userInfo?.profileUrl?.run { it.context?.openUrl(this) }
                }
                executePendingBindings()
            }

        }
    }

}

fun SimpleDraweeView.setGifController(url: String?, lowRes: String?) {
    controller = Fresco.newDraweeControllerBuilder()
            .setUri(Uri.parse(url))
            .setAutoPlayAnimations(true)
            .setOldController(controller)
            .setLowResImageRequest(ImageRequest.fromUri(lowRes))
            .build() as DraweeController
}