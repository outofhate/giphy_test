package com.outofdate.giphy.modules.main

import dagger.Binds
import dagger.Module

@Module
interface MainActivityModule {

    @Binds
    @MainActivityContract.Scope
    fun presenter(presenter: MainActivityPresenterImpl): MainActivityContract.Presenter

    @Binds
    @MainActivityContract.Scope
    fun interactor(interactor: MainActivityInteractorImpl): MainActivityContract.Interactor

}
