package com.outofdate.giphy.modules.details

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import com.outofdate.giphy.R
import com.outofdate.giphy.base.mvp.MvpFragment
import com.outofdate.giphy.databinding.GifDetailsBinding
import com.outofdate.giphy.modules.details.GifDetailsBinder.setupGif
import com.outofdate.giphy.modules.giflist.model.VM
import com.outofdate.giphy.utils.Params
import kotlin.reflect.KClass

class GifDetailsFragment :
        MvpFragment<
                GifDetailsBinding,
                GifDetailsContract.View,
                GifDetailsContract.Presenter,
                GifDetailsComponent
                >(),
        GifDetailsContract.View, SwipeRefreshLayout.OnRefreshListener {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.refresher.setOnRefreshListener(this)
        arguments?.run {
            presenter.getInfo(getString(Params.Bundles.imageId, null))
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun showGif(vm: VM?) {
        binding.vm = vm
        setupGif(binding.info, vm)
    }

    override fun onRefresh() {
        binding.refresher.isRefreshing = false
    }

    override fun showLoader() {
        binding.refresher.isRefreshing = true
    }

    override fun hideLoader() {
        binding.refresher.isRefreshing = false
    }

    override fun setupExitTransitions(): Boolean {
        return true
    }

    override fun setupEnterTransitions(): Boolean {
        return false
    }

    override val layout: Int = R.layout.gif_details

    override val componentClass: KClass<GifDetailsComponent> = GifDetailsComponent::class

    override fun getComponent(): GifDetailsComponent {
        return DaggerGifDetailsComponent
                .builder()
                .appComponent(getAppComponent())
                .build()
    }

    override fun injector(component: GifDetailsComponent) {
        component.inject(this)
    }
}
