package com.outofdate.giphy.modules.main

import com.outofdate.giphy.base.mvp.AbsMvpInteractor
import kotlinx.coroutines.experimental.CoroutineDispatcher
import javax.inject.Inject

class MainActivityInteractorImpl @Inject
constructor(main: CoroutineDispatcher) : AbsMvpInteractor(main),
        MainActivityContract.Interactor
