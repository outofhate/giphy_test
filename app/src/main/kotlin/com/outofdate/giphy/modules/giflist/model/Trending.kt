package com.outofdate.giphy.modules.giflist.model

data class Trending(val vm: List<VM>, val pagination: Pagination)

data class VM(
        val id: String,
        val preview: Image?,
        val original: Image,
        val userInfo: UserInfo?
) {
    val ratio: Float? = preview?.ratio
}

data class Image(val url: String, val movingPreview: String?, val w: Int, val h: Int, var anim: Boolean) {
    val ratio: Float = w.toFloat() / h.toFloat()
    override fun toString(): String {
        return "ListInfoViewModel(url='$url', w=$w, h=$h, ratio=$ratio)"
    }

}

data class UserInfo(
        val displayName: String,
        val username: String,
        val profileUrl: String,
        val verified: Boolean,
        val avatar: String
)

data class Pagination(
        val total: Int,
        val limit: Int,
        val offset: Int
)


