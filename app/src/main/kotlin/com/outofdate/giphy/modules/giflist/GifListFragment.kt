package com.outofdate.giphy.modules.giflist

import android.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.transition.*
import android.support.v4.view.AsyncLayoutInflater
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import com.outofdate.giphy.R
import com.outofdate.giphy.base.mvp.MvpFragment
import com.outofdate.giphy.databinding.DetailsBinding
import com.outofdate.giphy.databinding.FragmentGifsBinding
import com.outofdate.giphy.modules.details.GifDetailsBinder
import com.outofdate.giphy.modules.giflist.list.GifsController
import com.outofdate.giphy.modules.giflist.model.VM
import com.outofdate.giphy.utils.*
import javax.inject.Inject
import kotlin.reflect.KClass

class GifListFragment :
        MvpFragment<
                FragmentGifsBinding,
                GifsListContract.View,
                GifsListContract.Presenter,
                GifsListComponent
                >(),
        GifsListContract.View, SwipeRefreshLayout.OnRefreshListener {
    @Inject
    lateinit var controller: GifsController

    private var gifBinding: DetailsBinding? = null

    override fun renderGifs(gifs: List<VM>?) {
        controller.setData(gifs)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val lm: StaggeredGridLayoutManager = binding.gifs.layoutManager as StaggeredGridLayoutManager
        lm.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
        binding.gifs.setController(controller)
        binding.refresher.setOnRefreshListener(this)

        binding.gifs.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            val poss = IntArray(3)

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                lm.findLastCompletelyVisibleItemPositions(poss)
                if (poss.isNotEmpty())
                    presenter.setLastVisiblePosition(poss.last(), controller.adapter.itemCount)
            }
        })
        if (savedInstanceState == null)
            presenter.getGifs(-1)
        AsyncLayoutInflater(view.context).inflate(R.layout.details, binding.container) { view: View, i: Int, viewGroup: ViewGroup? ->
            view.invisible()
            viewGroup?.addView(view)
            gifBinding = DataBindingUtil.bind(view)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun showGifDetails(data: VM) {
        gifBinding?.run {
            GifDetailsBinder.setupGif(this, data)
            val view = binding.gifs.traverse {
                return@traverse if (it.transitionName() != data.id) {
                    it.visible()
                    false
                } else {
                    it.gone()
                    true
                }

            }
            val addTransition = TransitionSet().addTransition(ChangeBounds())
                    .setInterpolator(FastOutSlowInInterpolator())
                    .addTransition(ChangeTransform())
                    .addTransition(ChangeImageTransform())
            binding.overlay.setOnClickListener {
                TransitionManager.beginDelayedTransition(binding.root as ViewGroup,
                        addTransition)
                binding.overlay.invisible()
                actionImage.invisible()
                gifBinding?.root?.invisible()
                view?.visible()
            }

            view?.invisible()
            actionImage.visible()
            binding.overlay.visible()
            root.visible()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.list, menu)
        val switch = menu?.findItem(R.id.sw)?.actionView as Switch
        switch.setOnCheckedChangeListener { v, isChecked ->
            val data = ArrayList<VM>()
            controller.currentData?.forEach {
                val vm = it.copy()
                vm.preview?.anim = isChecked
                data.add(vm)
            }
            controller.setData(data)
        }
    }

    override fun onRefresh() {
        presenter.getGifs(-1)
    }

    override fun showLoader() {
        binding.refresher.isRefreshing = true
    }

    override fun hideLoader() {
        binding.refresher.isRefreshing = false
    }

    override fun setupEnterTransitions(): Boolean {
        return true
    }

    override fun setupExitTransitions(): Boolean {
        return false
    }

    override val layout: Int = R.layout.fragment_gifs

    override val componentClass: KClass<GifsListComponent> = GifsListComponent::class

    override fun getComponent(): GifsListComponent {
        return DaggerGifsListComponent
                .builder()
                .appComponent(getAppComponent())
                .build()
    }

    override fun injector(component: GifsListComponent) {
        component.inject(this)
    }

}
