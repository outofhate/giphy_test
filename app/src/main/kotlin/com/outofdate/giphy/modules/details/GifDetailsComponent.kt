package com.outofdate.giphy.modules.details

import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.DaggerComponent
import com.outofdate.giphy.base.internal.android.ComponentsHandlerModule
import com.outofdate.giphy.base.internal.net.servicemodules.GifApiModule
import dagger.Component

@GifDetailsContract.Scope
@Component(dependencies = [AppComponent::class],
        modules = [
            GifDetailsModule::class,
            ComponentsHandlerModule::class,
            GifApiModule::class
        ]
)
interface GifDetailsComponent : DaggerComponent {

    fun inject(activity: GifDetailsFragment)

}
