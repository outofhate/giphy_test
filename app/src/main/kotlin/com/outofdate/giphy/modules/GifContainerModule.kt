package com.outofdate.giphy.modules

import dagger.Module
import dagger.Provides

@Module
class GifContainerModule {

    val container: GifContainer = GifContainer()

    @Provides
    fun getGifContainer(): GifContainer {
        return container
    }
}
