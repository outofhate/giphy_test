package com.outofdate.giphy.modules.giflist

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.mvp.AbsMvpPresenter
import com.outofdate.giphy.modules.giflist.GifsListContract.*
import com.outofdate.giphy.modules.giflist.model.Pagination
import com.outofdate.giphy.modules.giflist.model.VM
import timber.log.Timber
import javax.inject.Inject

class GifsListPresenterImpl @Inject constructor(interactor: Interactor) : AbsMvpPresenter<View, Interactor>(interactor), Presenter {

    private var pagination: Pagination? = null

    private var offset: Int = 0

    override fun getGifs(offset: Int) {
        view?.showLoader()
        interactor.getGif(page, null, offset, CoroutineCallbackBase { trending, throwable ->
            view {
                if (trending != null) {
                    pagination = trending.pagination
                    renderGifs(trending.vm)
                }
                if (throwable != null) {
                    Timber.e(throwable)
                    showError(throwable.message)
                }
                hideLoader()
            }
        })
    }

    override fun setLastVisiblePosition(position: Int, count: Int) {
        val p = this@GifsListPresenterImpl
        pagination?.run {
            val checkPos = position + 5 >= offset
            if (checkPos) {
                p.offset = offset + page + 1
                getGifs(p.offset)
            }
        }
    }

    override fun showDetails(data: VM) {
        view?.showGifDetails(data)
    }

    companion object {
        private const val page = 40
    }

}
