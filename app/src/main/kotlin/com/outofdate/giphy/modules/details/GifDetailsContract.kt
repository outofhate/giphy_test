package com.outofdate.giphy.modules.details

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.mvp.Mvp
import com.outofdate.giphy.modules.giflist.model.VM

interface GifDetailsContract {

    interface View : Mvp.View {
        fun showGif(vm: VM?)
    }

    interface Presenter : Mvp.Presenter<View, Interactor> {
        fun getInfo(string: String?)
    }

    interface Interactor : Mvp.Interactor {
        fun getInfo(id: String?, callback: CoroutineCallbackBase<VM>)
    }


    @javax.inject.Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Scope
}

