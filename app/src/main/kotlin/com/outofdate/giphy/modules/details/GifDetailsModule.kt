package com.outofdate.giphy.modules.details

import dagger.Binds
import dagger.Module

@Module
interface GifDetailsModule {

    @Binds
    @GifDetailsContract.Scope
    fun presenter(presenter: GifDetailsPresenterImpl): GifDetailsContract.Presenter

    @Binds
    @GifDetailsContract.Scope
    fun interactor(interactor: GifDetailsInteractorImpl): GifDetailsContract.Interactor

}
