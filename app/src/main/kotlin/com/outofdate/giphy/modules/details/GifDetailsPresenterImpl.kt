package com.outofdate.giphy.modules.details

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.mvp.AbsMvpPresenter
import com.outofdate.giphy.modules.details.GifDetailsContract.*
import javax.inject.Inject

class GifDetailsPresenterImpl @Inject constructor(interactor: Interactor) : AbsMvpPresenter<View, Interactor>(interactor), Presenter {
    override fun getInfo(id: String?) {
        view?.showLoader()
        interactor.getInfo(id, CoroutineCallbackBase { vm, thr ->
            view {
                if (vm != null)
                    showGif(vm)
                if (thr != null)
                    showError(thr.message)
                hideLoader()
            }
        })
    }

}