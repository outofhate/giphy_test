package com.outofdate.giphy.modules.details

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.internal.net.services.GiphyGifsService
import com.outofdate.giphy.base.mvp.AbsMvpInteractor
import com.outofdate.giphy.modules.GifContainer
import com.outofdate.giphy.modules.details.GifDetailsContract.Interactor
import com.outofdate.giphy.modules.giflist.model.Image
import com.outofdate.giphy.modules.giflist.model.UserInfo
import com.outofdate.giphy.modules.giflist.model.VM
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.withContext
import javax.inject.Inject

class GifDetailsInteractorImpl @Inject
constructor(private val gifsService: GiphyGifsService, private val container: GifContainer, main: CoroutineDispatcher) : AbsMvpInteractor(main),
        Interactor {
    override fun getInfo(id: String?, callback: CoroutineCallbackBase<VM>) {
        if (id != null) {
            async {
                val job = gifsService.byId(id)
                var v: VM? = null
                job.invokeOnCompletion {
                    async {
                        withContext(main) {
                            callback.invoke(v, it)
                        }
                    }
                }
                val data = job.await()
                        ?.data

                v = data?.let {
                    VM(it.id, null,
                            it.images.run {
                                Image(it.images.original.url, it.images.originalStill.url, it.images.original.width, it.images.original.height, true)
                            },
                            it.user?.run {
                                UserInfo(displayName, username, profileUrl, isVerified, avatarUrl)
                            })
                }
            }
        } else {
            if (container.vm == null) {
                callback.invoke(null, NullPointerException("No saved gif in container"))
            } else {
                callback.invoke(container.vm, null)
            }
        }
    }
}
