package com.outofdate.giphy.modules.main

import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.DaggerComponent
import com.outofdate.giphy.base.internal.android.ComponentsHandlerModule
import dagger.Component

@MainActivityContract.Scope
@Component(dependencies = [AppComponent::class],
        modules = [
            MainActivityModule::class,
            ComponentsHandlerModule::class
        ]
)
interface MainActivityComponent : DaggerComponent {

    fun inject(activity: MainActivity)

}
