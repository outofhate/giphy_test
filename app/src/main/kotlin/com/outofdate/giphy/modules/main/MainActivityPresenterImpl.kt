package com.outofdate.giphy.modules.main

import com.outofdate.giphy.base.mvp.AbsMvpPresenter
import com.outofdate.giphy.modules.main.MainActivityContract.*
import javax.inject.Inject

class MainActivityPresenterImpl @Inject constructor(interactor: Interactor) : AbsMvpPresenter<View, Interactor>(interactor), Presenter {

}