package com.outofdate.giphy.modules.giflist

import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.DaggerComponent
import com.outofdate.giphy.base.internal.android.ComponentsHandlerModule
import com.outofdate.giphy.base.internal.net.servicemodules.GifApiModule
import com.outofdate.giphy.modules.giflist.list.GifModel
import dagger.Component

@GifsListContract.Scope
@Component(dependencies = [AppComponent::class],
        modules = [GifsListModule::class,
            ComponentsHandlerModule::class,
            GifApiModule::class]
)
interface GifsListComponent : DaggerComponent {

    fun inject(fragment: GifListFragment)

    fun inject(model: GifModel)
}
