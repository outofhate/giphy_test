package com.outofdate.giphy.modules.main

import android.os.Bundle
import com.facebook.drawee.backends.pipeline.Fresco
import com.outofdate.giphy.R
import com.outofdate.giphy.base.mvp.MvpActivity
import com.outofdate.giphy.databinding.ActivityMainBinding
import kotlin.reflect.KClass

class MainActivity :
        MvpActivity<
                ActivityMainBinding,
                MainActivityContract.View,
                MainActivityContract.Presenter,
                MainActivityComponent>(),
        MainActivityContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        Fresco.initialize(this)
        super.onCreate(savedInstanceState)
    }

    override fun showLoader() {

    }

    override fun hideLoader() {

    }

    override val layout: Int = R.layout.activity_main

    override val componentClass: KClass<MainActivityComponent> = MainActivityComponent::class

    override fun getComponent(): MainActivityComponent {
        return DaggerMainActivityComponent
                .builder()
                .appComponent(getAppComponent())
                .build()
    }

    override fun injector(component: MainActivityComponent) {
        component.inject(this)
    }
}
