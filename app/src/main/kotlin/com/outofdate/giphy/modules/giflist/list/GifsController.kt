package com.outofdate.giphy.modules.giflist.list

import com.airbnb.epoxy.TypedEpoxyController
import com.outofdate.giphy.modules.giflist.GifsListContract
import com.outofdate.giphy.modules.giflist.model.VM
import javax.inject.Inject

@GifsListContract.Scope
class GifsController @Inject constructor() : TypedEpoxyController<List<VM>>() {

    init {
        setFilterDuplicates(true)
    }

    override fun buildModels(data: List<VM>?) {
        data?.forEach { GifModel(it).addTo(this) }
    }

}


