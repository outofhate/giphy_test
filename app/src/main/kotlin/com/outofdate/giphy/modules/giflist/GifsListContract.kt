package com.outofdate.giphy.modules.giflist

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.mvp.Mvp
import com.outofdate.giphy.modules.giflist.model.Trending
import com.outofdate.giphy.modules.giflist.model.VM

interface GifsListContract {

    interface View : Mvp.View {
        fun renderGifs(gifs: List<VM>?)
        fun showGifDetails(data: VM)
    }

    interface Presenter : Mvp.Presenter<View, Interactor> {
        fun getGifs(offset: Int)
        fun setLastVisiblePosition(position: Int, count: Int)
        fun showDetails(data: VM)
    }

    interface Interactor : Mvp.Interactor {
                fun getGif(
                limit: Int,
                rating: String?,
                off: Int,
                callback: CoroutineCallbackBase<Trending>
        )
    }

    @javax.inject.Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Scope


}
