package com.outofdate.giphy.modules.giflist

import com.outofdate.giphy.modules.giflist.GifsListContract.Interactor
import com.outofdate.giphy.modules.giflist.GifsListContract.Presenter
import dagger.Binds
import dagger.Module

@Module
interface GifsListModule {

    @Binds
    @GifsListContract.Scope
    fun presenter(presenter: GifsListPresenterImpl): Presenter

    @Binds
    @GifsListContract.Scope
    fun interactor(interactor: GifListInteractorImpl): Interactor

}
