package com.outofdate.giphy.modules.giflist.list

import com.outofdate.giphy.R
import com.outofdate.giphy.base.epoxy.BaseEpoxyModel
import com.outofdate.giphy.databinding.ViewGifBinding
import com.outofdate.giphy.modules.GifContainer
import com.outofdate.giphy.modules.details.setGifController
import com.outofdate.giphy.modules.giflist.DaggerGifsListComponent
import com.outofdate.giphy.modules.giflist.GifsListComponent
import com.outofdate.giphy.modules.giflist.GifsListContract
import com.outofdate.giphy.modules.giflist.model.VM
import com.outofdate.giphy.utils.setupNavTransition
import com.outofdate.giphy.utils.transitionName
import javax.inject.Inject
import kotlin.reflect.KClass

class GifModel(data: VM) : BaseEpoxyModel<ViewGifBinding, VM, GifsListComponent>(data) {
    @Inject
    lateinit var gifContainer: GifContainer

    @Inject
    lateinit var presenter: GifsListContract.Presenter

    init {
        id(data.id, data.preview?.anim.toString())
    }

    override fun bind(binding: ViewGifBinding) {
        binding.actionImage.run {
            transitionName(data.id)
            aspectRatio = data.preview?.ratio!!
            if (data.preview.anim)
                this.setGifController(data.preview.movingPreview, data.preview.url)
            else
                setImageURI(data.preview.url)
            setupNavTransition {
                gifContainer.vm = data
            }
            setOnLongClickListener {
                presenter.showDetails(data)
                true
            }

        }
    }

    override fun unbind(binding: ViewGifBinding) {
        binding.actionImage.setImageURI("")
    }

    override fun getDefaultLayout(): Int = R.layout.view_gif


    override val componentClass: KClass<GifsListComponent> = GifsListComponent::class

    override fun getComponent(): GifsListComponent {
        return DaggerGifsListComponent
                .builder()
                .appComponent(getAppComponent())
                .build()
    }

    override fun injector(component: GifsListComponent) {
        component.inject(this)
    }
}

