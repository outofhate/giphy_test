package com.outofdate.giphy.modules.giflist

import com.outofdate.giphy.base.internal.CoroutineCallbackBase
import com.outofdate.giphy.base.internal.net.model.TrendingResponse
import com.outofdate.giphy.base.internal.net.services.GiphyGifsService
import com.outofdate.giphy.base.mvp.AbsMvpInteractor
import com.outofdate.giphy.modules.GifContainer
import com.outofdate.giphy.modules.giflist.GifsListContract.Interactor
import com.outofdate.giphy.modules.giflist.model.*
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.withContext
import timber.log.Timber
import javax.inject.Inject

class GifListInteractorImpl @Inject constructor(private val gif: GiphyGifsService, private val container: GifContainer, main: CoroutineDispatcher) : AbsMvpInteractor(main), Interactor {
    private var offset: Int = -1

    private val vms: ArrayList<VM> = ArrayList()
    var job: Deferred<*>? = null
    override fun getGif(limit: Int, rating: String?, off: Int, callback: CoroutineCallbackBase<Trending>) {
        Timber.d(" $off $offset")
        val canRunJob = job?.run { off != offset && (isCompleted || !isActive) } ?: true
        if (canRunJob) {
            offset = if (off == -1) 0 else off
            job = async {
                Thread.sleep(400)
                val job = gif.trending(limit, rating, offset)
                val response: TrendingResponse? = job.await()
                val data = response?.run {
                    val list = this.data.map {
                        Timber.d("Id: ${it.id}")
                        it.run {
                            VM(
                                    id,
                                    images.previewStill.run {
                                        Image(url, images.previewGif.url, width, height, false)
                                    },
                                    images.original.run {
                                        Image(url, images.originalStill.url, width, height, true)
                                    },
                                    user?.run {
                                        UserInfo(displayName, username, profileUrl, isVerified, avatarUrl)
                                    }
                            )
                        }
                    }
                    if (off == 0)
                        vms.clear()
                    vms.addAll(list)
                    Trending(vms, pagination.run {
                        Pagination(totalCount, count, offset)
                    })
                }
                job.invokeOnCompletion {
                    post(callback, data, it)
                }
            }.disposeOnComplete {
                post(callback, null, it)
            }
        } else
            callback.invoke(null, null)
    }

    private fun post(callback: CoroutineCallbackBase<Trending>,
                     data: Trending?,
                     it: Throwable?) {
        async {
            withContext(main) {
                callback.invoke(data, it)
            }
        }
    }

}

