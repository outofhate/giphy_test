/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.outofdate.giphy.utils.navigation

import android.content.Context
import android.os.Bundle
import android.support.annotation.NavigationRes
import android.support.v4.app.Fragment
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.navigation.*
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.NavHostFragment
import com.outofdate.giphy.R
import ru.foodfox.client.utils.navigation.TransitionDestination
import ru.foodfox.client.utils.navigation.TransitionFragmentNavigator

/**
 * NavHostFragment provides an area within your layout for self-contained navigation to occur.
 *
 *
 * NavHostFragment is intended to be used as the content area within a layout resource
 * defining your app's chrome around it, e.g.:
 *
 * <pre class="prettyprint">
 * &lt;android.support.v4.widget.DrawerLayout
 * xmlns:android="http://schemas.android.com/apk/res/android"
 * xmlns:app="http://schemas.android.com/apk/res-auto"
 * android:layout_width="match_parent"
 * android:layout_height="match_parent"&gt;
 * &lt;fragment
 * android:layout_width="match_parent"
 * android:layout_height="match_parent"
 * android:id="@+id/my_nav_host_fragment"
 * android:name="androidx.navigation.fragment.NavHostFragment"
 * app:navGraph="@xml/nav_sample"
 * app:defaultNavHost="true" /&gt;
 * &lt;android.support.design.widget.NavigationView
 * android:layout_width="wrap_content"
 * android:layout_height="match_parent"
 * android:layout_gravity="start"/&gt;
 * &lt;/android.support.v4.widget.DrawerLayout&gt;
</pre> *
 *
 *
 * Each NavHostFragment has a [NavController] that defines valid navigation within
 * the navigation host. This includes the [navigation graph][NavGraph] as well as navigation
 * state such as current location and back stack that will be saved and restored along with the
 * NavHostFragment itself.
 *
 *
 * NavHostFragments register their navigation controller at the root of their view subtree
 * such that any descendant can obtain the controller instance through the [Navigation]
 * helper class's methods such as [Navigation.findNavController]. MenuCartView event listener
 * implementations such as [View.OnClickListener] within navigation destination
 * fragments can use these helpers to setupNav based on user interaction without creating a tight
 * coupling to the navigation host.
 */
class NavHostTransitionFragment : NavHostFragment(), NavHost {

    private var navController: NavController? = null

    // State that will be saved and restored
    private var defaultNavHost: Boolean = false

    /**
     * Returns the [navigation controller][NavController] for this navigation host.
     * This method will return null until this host fragment's [.onCreate]
     * has been called and it has had an opportunity to restore from a previous instance state.
     *
     * @return this host's navigation controller
     * @throws IllegalStateException if called before [.onCreate]
     */
    override fun getNavController(): NavController {
        if (navController == null) {
            throw IllegalStateException("NavController is not available before onCreate()")
        }
        return navController!!
    }

    /**
     * Set a [NavGraph] for this navigation host's [NavController] by resource id.
     * The existing graph will be replaced.
     *
     * @param graphResId resource id of the navigation graph to inflate
     */
    override fun setGraph(@NavigationRes graphResId: Int) {
        if (navController == null) {
            var args = arguments
            if (args == null) {
                args = Bundle()
            }
            args.putInt(KEY_GRAPH_ID, graphResId)
            arguments = args
        } else {
            navController!!.setGraph(graphResId)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // TODO This feature should probably be a first-class feature of the Fragment system,
        // but it can stay here until we can add the necessary attr resources to
        // the fragment lib.
        if (defaultNavHost) {
            requireFragmentManager().beginTransaction()
                    .setPrimaryNavigationFragment(this)
                    .commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val context = requireContext()

        navController = NavController(context)
        navController!!.navigatorProvider.addNavigator(createTransitionFragmentNavigator())

        var navState: Bundle? = null
        if (savedInstanceState != null) {
            navState = savedInstanceState.getBundle(KEY_NAV_CONTROLLER_STATE)
            if (savedInstanceState.getBoolean(KEY_DEFAULT_NAV_HOST, false)) {
                defaultNavHost = true
                requireFragmentManager().beginTransaction()
                        .setPrimaryNavigationFragment(this)
                        .commit()
            }
        }
        if (navState != null) {
            // Navigation controller state overrides arguments
            navController!!.restoreState(navState)
        } else {
            val args = arguments
            val graphId = args?.getInt(KEY_GRAPH_ID) ?: 0
            if (graphId != 0) {
                navController!!.setGraph(graphId)
            } else {
                navController!!.setMetadataGraph()
            }
        }
    }

    /**
     * Create the FragmentNavigator that this NavHostFragment will use. By default, this uses
     * [FragmentNavigator], which replaces the entire contents of the NavHostFragment.
     *
     *
     * This is only called once in [.onCreate] and should not be called directly by
     * subclasses.
     *
     * @return a new instance of a FragmentNavigator
     */

    protected fun createTransitionFragmentNavigator(): Navigator<out TransitionDestination> {
        return TransitionFragmentNavigator(requireContext(), childFragmentManager, id)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val frameLayout = FrameLayout(inflater.context)
        // When added via XML, this has no effect (since this FrameLayout is given the ID
        // automatically), but this ensures that the MenuCartView exists as part of this Fragment's MenuCartView
        // hierarchy in cases where the NavHostFragment is added programmatically as is required
        // for child fragment transactions
        frameLayout.id = id
        return frameLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (view !is ViewGroup) {
            throw IllegalStateException("created host view $view is not a ViewGroup")
        }
        // When added via XML, the parent is null and our view is the root of the NavHostFragment
        // but when added programmatically, we need to set the NavController on the parent - i.e.,
        // the MenuCartView that has the ID matching this NavHostFragment.
        val rootView = if (view.getParent() != null) view.getParent() as View else view
        Navigation.setViewNavController(rootView, navController)
    }

    override fun onInflate(context: Context, attrs: AttributeSet, savedInstanceState: Bundle?) {
        super.onInflate(context, attrs, savedInstanceState)

        val a = context.obtainStyledAttributes(attrs, R.styleable.NavHostTransitionFragment)
        val graphId = a.getResourceId(R.styleable.NavHostTransitionFragment_navGraph, 0)
        val defaultHost = a.getBoolean(R.styleable.NavHostTransitionFragment_defaultNavHost, false)

        if (graphId != 0) {
            setGraph(graphId)
        }
        if (defaultHost) {
            defaultNavHost = true
        }
        a.recycle()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val navState = navController!!.saveState()
        if (navState != null) {
            outState.putBundle(KEY_NAV_CONTROLLER_STATE, navState)
        }
        if (defaultNavHost) {
            outState.putBoolean(KEY_DEFAULT_NAV_HOST, true)
        }
    }

    companion object {
        private val KEY_GRAPH_ID = "eda-nav:fragment:graphId"
        private val KEY_NAV_CONTROLLER_STATE = "eda-nav:fragment:navControllerState"
        private val KEY_DEFAULT_NAV_HOST = "eda-nav:fragment:defaultHost"

        /**
         * Find a [NavController] given a local [Fragment].
         *
         *
         * This method will locate the [NavController] associated with this Fragment,
         * looking first for a [NavHostTransitionFragment] along the given Fragment's parent chain.
         * If a [NavController] is not found, this method will look for one along this
         * Fragment's [view hierarchy][Fragment.getView] as specified by
         * [Navigation.findNavController].
         *
         * @param fragment the locally scoped Fragment for navigation
         * @return the locally scoped [NavController] for navigating from this [Fragment]
         * @throws IllegalStateException if the given Fragment does not correspond with a
         * [NavHost] or is not within a NavHost.
         */
        fun findNavController(fragment: Fragment): NavController {
            var findFragment: Fragment? = fragment
            while (findFragment != null) {
                if (findFragment is NavHostTransitionFragment) {
                    return findFragment.getNavController()
                }
                val primaryNavFragment = findFragment.requireFragmentManager()
                        .primaryNavigationFragment
                if (primaryNavFragment is NavHostTransitionFragment) {
                    return primaryNavFragment.getNavController()
                }
                findFragment = findFragment.parentFragment
            }

            // Try looking for one associated with the view instead, if applicable
            val view = fragment.view
            if (view != null) {
                return Navigation.findNavController(view)
            }
            throw IllegalStateException("Fragment " + fragment
                    + " does not have a NavController set")
        }

        /**
         * Create a new NavHostFragment instance with an inflated [NavGraph] resource.
         *
         * @param graphResId resource id of the navigation graph to inflate
         * @return a new NavHostFragment instance
         */
        fun create(@NavigationRes graphResId: Int): NavHostTransitionFragment {
            var b: Bundle? = null
            if (graphResId != 0) {
                b = Bundle()
                b.putInt(KEY_GRAPH_ID, graphResId)
            }

            val result = NavHostTransitionFragment()
            if (b != null) {
                result.arguments = b
            }
            return result
        }
    }
}
