package com.outofdate.giphy.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

object IntentUtils {

}
public fun Context.openUrl(url: String) {
    try {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    } catch (ignore: Exception) {
    }

}
