package com.outofdate.giphy.utils.animation

import android.animation.TimeInterpolator
import android.annotation.SuppressLint
import android.transition.Transition
import android.transition.TransitionSet

@SuppressLint("NewApi")
open class CommonTransitionSet : TransitionSet() {
    init {
        interpolator = AnimationsConfig.decelerateInterpolator
    }

    override fun addTransition(transition: Transition): TransitionSet {
        transition.interpolator = interpolator
        return super.addTransition(transition)
    }

    override fun setInterpolator(interpolator: TimeInterpolator): TransitionSet {
        for (i in 0 until transitionCount) {
            getTransitionAt(i).interpolator = interpolator
        }
        return super.setInterpolator(interpolator)
    }
}
