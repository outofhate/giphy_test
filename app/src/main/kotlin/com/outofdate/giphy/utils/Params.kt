package com.outofdate.giphy.utils

object Params {
    object Prefs {
        const val main = "main_prefs"
    }
    object Net {
        const val apiKey = "api_key"
        const val giphyApiKey = "giphy_api_key"
    }
    object Bundles {
        const val imageId = "gif_id"
    }
}
