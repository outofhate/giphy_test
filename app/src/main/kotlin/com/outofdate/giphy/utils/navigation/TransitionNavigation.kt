package ru.foodfox.client.utils.navigation

import android.support.annotation.IdRes
import android.view.View
import androidx.navigation.Navigation
import com.outofdate.giphy.utils.BundleUtils.getBundle

object TransitionNavigation {

    fun navigate(view: View, @IdRes id: Int, key: String?, value: String?, vararg transitionViews: View) {
        navigate(view, id, key, value) { dest ->
            dest.clearViews()
            transitionViews.forEach {
                dest.addView(it)
            }
        }
    }

    fun navigate(view: View) {
        navigate(view, view.id, null, null, view)
    }

    fun navigate(view: View, @IdRes id: Int, transitionViews: MutableList<View>, key: String?, value: String?) {
        navigate(view, id, key, value) { it.setViews(transitionViews) }
    }

    private fun navigate(view: View, id: Int, key: String?, value: String?, add: (TransitionDestination) -> Unit) {
        Navigation.findNavController(view)
                .run {
                    graph.getAction(id)?.destinationId?.let {
                        val dest = graph.findNode(it) as TransitionDestination
                        add(dest)
                        dest.navigate(getBundle(key, value), null)
                    }

                }
    }
}
