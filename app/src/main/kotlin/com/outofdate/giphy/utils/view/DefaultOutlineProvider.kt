package com.outofdate.giphy.utils.view

import android.annotation.TargetApi
import android.os.Build
import android.view.View
import android.view.ViewOutlineProvider

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
class DefaultOutlineProvider : ViewOutlineProvider {
    private var alpha = 0.3f

    constructor() {}

    constructor(alpha: Float) {
        this.alpha = alpha
    }

    override fun getOutline(view: View, outline: android.graphics.Outline) {
        val background = view.background
        if (background != null) {
            background.getOutline(outline)
            outline.alpha = alpha
        }
    }

    companion object {

        private val provider: DefaultOutlineProvider = DefaultOutlineProvider()

        fun get(): ViewOutlineProvider {
            return provider
        }
    }
}

