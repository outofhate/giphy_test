package com.outofdate.giphy.utils

import android.os.Build

object Versions {

    fun api(api: Int, runnable: () -> Unit): Boolean {
        val sdk = Build.VERSION.SDK_INT >= api
        if (sdk) {
            runnable.invoke()
        }
        return sdk
    }

    fun apiBefore(api: Int, runnable: () -> Unit): Boolean {
        val sdk = Build.VERSION.SDK_INT < api
        if (sdk) {
            runnable.invoke()
        }
        return sdk
    }

    fun a21(runnable: () -> Unit): Boolean {
        return lollipop(runnable)
    }

    fun a23(runnable: () -> Unit): Boolean {
        return marshmallow(runnable)
    }

    fun a19(runnable: () -> Unit): Boolean {
        return api(Build.VERSION_CODES.KITKAT, runnable)
    }

    fun marshmallow(runnable: () -> Unit): Boolean {
        return api(Build.VERSION_CODES.M, runnable)
    }

    fun marshmallow(ifProvidedApi: () -> Unit, elseProvidedApi: () -> Unit) {
        if (!api(Build.VERSION_CODES.M, ifProvidedApi)) api(Build.VERSION_CODES.M, elseProvidedApi)
    }

    fun lollipop(runnable: () -> Unit): Boolean {
        return api(Build.VERSION_CODES.LOLLIPOP, runnable)
    }

    fun oreo(runnable: () -> Unit): Boolean {
        return api(Build.VERSION_CODES.O, runnable)
    }

}