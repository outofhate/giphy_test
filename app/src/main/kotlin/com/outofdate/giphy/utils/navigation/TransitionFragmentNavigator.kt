package ru.foodfox.client.utils.navigation

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.View
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.NavigatorProvider
import androidx.navigation.fragment.FragmentNavigator
import com.outofdate.giphy.R
import ru.foodfox.client.utils.navigation.TransitionFragmentNavigator.Companion.NAVIGATOR
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

@Navigator.Name(NAVIGATOR)
class TransitionFragmentNavigator(private val context: Context, private val manager: FragmentManager, private val containerId: Int) : Navigator<TransitionDestination>() {

    private val TAG = "TransitionFragmentNavigator"
    private var backStack = ArrayDeque<Int>()
    private var lastBackStackCount = 1
    private var pendingBackStackOperations = 0

    private val onBackStackChangedListener = FragmentManager.OnBackStackChangedListener {
        val newCount = manager.backStackEntryCount + 1
        if (pendingBackStackOperations > 0 && newCount <= lastBackStackCount + pendingBackStackOperations) {
            pendingBackStackOperations -= newCount - lastBackStackCount
            lastBackStackCount = newCount
            return@OnBackStackChangedListener
        }
        lastBackStackCount = newCount
        if (newCount < backStack.size) {
            while (backStack.size > newCount) {
                backStack.removeLast()
            }
            val destId = if (backStack.isEmpty()) 0 else backStack.peekLast()
            dispatchOnNavigatorNavigated(destId, Navigator.BACK_STACK_DESTINATION_POPPED)
        }
    }

    override fun onActive() {
        manager.addOnBackStackChangedListener(onBackStackChangedListener)
    }

    override fun onInactive() {
        manager.removeOnBackStackChangedListener(onBackStackChangedListener)
    }

    override fun popBackStack(): Boolean {
        if (backStack.isEmpty()) {
            return false
        }
        if (manager.isStateSaved) {
            return false
        }
        var popped = false
        if (manager.backStackEntryCount > 0) {
            manager.popBackStack()
            popped = true
        }
        backStack.removeLast()
        val destId = if (backStack.isEmpty()) 0 else backStack.peekLast()
        dispatchOnNavigatorNavigated(destId, Navigator.BACK_STACK_DESTINATION_POPPED)
        return popped
    }

    override fun createDestination(): TransitionDestination {
        return TransitionDestination(this)
    }

    private fun getBackStackName(@IdRes destinationId: Int): String {
        // This gives us the resource name if it exists,
        // or just the destinationId if it doesn't exist
        return try {
            context.resources.getResourceName(destinationId)
        } catch (e: Resources.NotFoundException) {
            Integer.toString(destinationId)
        }
    }

    fun navigate(dest: TransitionDestination) {
        navigate(dest, null, null)
    }

    fun navigate(dest: TransitionDestination, args: Bundle?) {
        navigate(dest, args, null)
    }

    override fun navigate(destination: TransitionDestination, args: Bundle?,
                          navOptions: NavOptions?) {
        if (manager.isStateSaved) {
            return
        }
        val frag = destination.createFragment(args)
        val ft = manager.beginTransaction()

        for (v in destination.views)
            ViewCompat.getTransitionName(v)?.let { ft.addSharedElement(v, it) }
        var enterAnim = navOptions?.enterAnim ?: -1
        var exitAnim = navOptions?.exitAnim ?: -1
        var popEnterAnim = navOptions?.popEnterAnim ?: -1
        var popExitAnim = navOptions?.popExitAnim ?: -1
        if (enterAnim != -1 || exitAnim != -1 || popEnterAnim != -1 || popExitAnim != -1) {
            enterAnim = if (enterAnim != -1) enterAnim else 0
            exitAnim = if (exitAnim != -1) exitAnim else 0
            popEnterAnim = if (popEnterAnim != -1) popEnterAnim else 0
            popExitAnim = if (popExitAnim != -1) popExitAnim else 0
            ft.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim)
        }

        ft.replace(containerId, frag)
        ft.setPrimaryNavigationFragment(frag)

        @IdRes val destId = destination.id
        val initialNavigation = backStack.isEmpty()
        val isClearTask = navOptions != null && navOptions.shouldClearTask()
        val isSingleTopReplacement = (navOptions != null && !initialNavigation
                && navOptions.shouldLaunchSingleTop()
                && backStack.peekLast() == destId)

        val backStackEffect: Int
        if (initialNavigation || isClearTask) {
            backStackEffect = Navigator.BACK_STACK_DESTINATION_ADDED
        } else if (isSingleTopReplacement) {
            // Single Top means we only want one instance on the back stack
            if (backStack.size > 1) {
                // If the Fragment to be replaced is on the FragmentManager's
                // back stack, a simple replace() isn't enough so we
                // remove it from the back stack and put our replacement
                // on the back stack in its place
                manager.popBackStack()
                ft.addToBackStack(getBackStackName(destId))
                pendingBackStackOperations++
            }
            backStackEffect = Navigator.BACK_STACK_UNCHANGED
        } else {
            ft.addToBackStack(getBackStackName(destId))
            pendingBackStackOperations++
            backStackEffect = Navigator.BACK_STACK_DESTINATION_ADDED
        }
        if (destination.views.isEmpty())
            ft.setReorderingAllowed(true)
        destination.views.clear()
        ft.commitAllowingStateLoss()
        // The commit succeeded, update our view of the world
        if (backStackEffect == Navigator.BACK_STACK_DESTINATION_ADDED) {
            backStack.add(destId)
        }
        dispatchOnNavigatorNavigated(destId, backStackEffect)
    }

    override fun onSaveState(): Bundle? {
        val b = Bundle()
        val backStack = IntArray(backStack.size)
        var index = 0
        for (id in this.backStack) {
            backStack[index++] = id!!
        }
        b.putIntArray(KEY_BACK_STACK_IDS, backStack)
        return b
    }

    override fun onRestoreState(savedState: Bundle) {
        val backStack = savedState.getIntArray(KEY_BACK_STACK_IDS)
        if (backStack != null) {
            this.backStack.clear()
            for (destId in backStack) {
                this.backStack.add(destId)
            }
            lastBackStackCount = this.backStack.size
        }
    }


    companion object {
        const val KEY_BACK_STACK_IDS = "ru-foodfox-client-utils-navigation:navigator:backStackIds"
        const val NAVIGATOR = "fragment"
    }


}

class TransitionDestination
(fragmentNavigator: Navigator<out FragmentNavigator.Destination>) : FragmentNavigator.Destination(fragmentNavigator) {

    private var fragmentClass: Class<out Fragment>? = null
    internal var views: MutableList<View> = ArrayList()

    constructor(navigatorProvider: NavigatorProvider) : this(navigatorProvider.getNavigator<TransitionDestination, TransitionFragmentNavigator>(TransitionFragmentNavigator::class.java))


    override fun onInflate(context: Context, attrs: AttributeSet) {
        super.onInflate(context, attrs)
        val a = context.resources.obtainAttributes(attrs,
                R.styleable.FragmentNavigator)
        setFragmentClass(getFragmentClassByName(context, a.getString(
                R.styleable.FragmentNavigator_android_name)))
        a.recycle()
    }

    fun addView(view: View): TransitionDestination {
        if (!views.contains(view)) {
            views.add(view)
        }
        return this
    }

    fun setViews(views: List<View>): TransitionDestination {
        this.views.clear()
        this.views.addAll(views)
        return this
    }

    private fun getFragmentClassByName(context: Context, name: String?): Class<out Fragment> {
        var n: String? = name
        if (n != null && n[0] == '.') {
            n = context.packageName + n
        }
        var clazz: Class<out Fragment>? = fragmentClasses[n]
        if (clazz == null) {
            try {
                clazz = Class.forName(n, true,
                        context.classLoader) as Class<out Fragment>
                if (n != null)
                    fragmentClasses[n] = clazz
            } catch (e: ClassNotFoundException) {
                throw RuntimeException(e)
            }

        }
        return clazz
    }

    override fun getNavigator(): TransitionFragmentNavigator {
        return super.getNavigator() as TransitionFragmentNavigator
    }

    override fun setFragmentClass(clazz: Class<out Fragment>): TransitionDestination {
        fragmentClass = clazz
        return this
    }

    override fun getFragmentClass(): Class<out Fragment> {
        return fragmentClass!!
    }

    override fun createFragment(args: Bundle?): Fragment {
        val clazz = getFragmentClass() ?: throw IllegalStateException("fragment class not set")

        val f: Fragment
        try {
            f = clazz.newInstance()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

        if (args != null) {
            f.arguments = args
        }
        return f
    }

    fun clearViews() {
        views.clear()
    }

    override fun toString(): String {
        return "TransitionDestination(Label = $label, id = $id)"
    }


    companion object {
        private val fragmentClasses = HashMap<String, Class<out Fragment>>()
    }
}


