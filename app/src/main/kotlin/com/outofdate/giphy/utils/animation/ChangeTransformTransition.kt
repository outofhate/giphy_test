package com.outofdate.giphy.utils.animation

import android.annotation.TargetApi
import android.os.Build
import android.transition.ChangeBounds
import android.transition.ChangeTransform
import android.transition.TransitionSet
import android.view.animation.Transformation

@TargetApi(Build.VERSION_CODES.KITKAT)
class ChangeTransformTransition @TargetApi(Build.VERSION_CODES.LOLLIPOP)
constructor(enter: Boolean, duration: Long) : CommonTransitionSet() {

    init {
        ordering = TransitionSet.ORDERING_TOGETHER
        setDuration(duration)
        Transformation()
        val changeTransform = ChangeTransform()
        val changeBounds = ChangeBounds()
        addTransition(changeTransform)
        addTransition(changeBounds)
    }
}
