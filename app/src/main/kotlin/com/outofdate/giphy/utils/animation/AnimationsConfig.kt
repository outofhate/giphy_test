package com.outofdate.giphy.utils.animation

import android.view.animation.DecelerateInterpolator

object AnimationsConfig {

    val decelerateInterpolator = DecelerateInterpolator(3f)
    const val DURATION = 600L
}