package com.outofdate.giphy.utils

import android.graphics.Typeface
import android.os.Bundle
import android.support.annotation.DimenRes
import android.support.v4.view.ViewCompat.getTransitionName
import android.support.v4.view.ViewCompat.setTransitionName
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.TextView
import androidx.navigation.Navigation
import ru.foodfox.client.utils.navigation.TransitionNavigation


inline fun View.layout(crossinline run: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            run()
        }
    })
    requestLayout()
}

inline fun View.layoutNoRequest(crossinline run: () -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            viewTreeObserver.removeOnGlobalLayoutListener(this)
            run()
        }
    })
}

fun View.getScreenW(): Int {
    return resources.displayMetrics.widthPixels
}

fun View.getScreenH(): Int {
    return resources.displayMetrics.heightPixels
}

fun View.dimenInt(@DimenRes id: Int): Int {
    return resources.getDimensionPixelSize(id)
}

fun View.dimenFloat(@DimenRes id: Int): Float {
    return resources.getDimension(id)
}

fun View.gridToPx(grid: Int): Float {
    return dpToPx(grid * 8)
}

fun View.dpToPx(dp: Int): Float {
    return dp * resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT
}


fun View.transitionName(name: String) {
    setTransitionName(this, name)
}

fun View.transitionName(): String {
    return getTransitionName(this) ?: ""
}

fun View.traverse(predicate: (View) -> Boolean): View? {
    if (!predicate.invoke(this)) {
        if (this is ViewGroup) {
            for (i in 0 until childCount) {
                val traverse = getChildAt(i).traverse(predicate)
                if (traverse != null)
                return traverse
            }
        } else {
            return null
        }
    } else {
        return this
    }
    return null
}

fun ViewGroup.forEach(v: (View) -> View?): View {
    for (i in 0 until this.childCount) {
        val view = getChildAt(i)
        v.invoke(view)
    }
    return this
}


fun trav(parent: ViewGroup, font: Typeface) {
    for (i in 0 until parent.childCount) {
        val child = parent.getChildAt(i)
        if (child is ViewGroup) {
            trav(child, font)
        } else if (child != null) {
            Log.d("menfis", child.toString())
            if (child.javaClass == TextView::class.java) {
                (child as TextView).typeface = font
            }
        }
    }
}

fun View.isVisible(): Boolean {
    return visibility == View.VISIBLE
}

fun View.isGone(): Boolean {
    return visibility == View.GONE
}

fun View.isInvisible(): Boolean {
    return visibility == View.INVISIBLE
}

fun View.gone() {
    if (!isGone()) visibility = View.GONE
}

fun View.visible() {
    if (!isVisible()) visibility = View.VISIBLE
}

fun View.invisible() {
    if (!isInvisible()) visibility = View.INVISIBLE
}

fun View.setupNav() {
    setupNav(null)
}

fun View.setupNav(id: Int) {
    setupNav(id, null)
}

fun View.setupNav(id: Int, args: Bundle?) {
    setOnClickListener { Navigation.findNavController(this).navigate(id, args) }
}

fun View.setupNav(args: Bundle?) {
    setOnClickListener { Navigation.findNavController(this).navigate(id, args) }
}

fun View.setupNavUp() {
    setOnClickListener { Navigation.findNavController(this).navigateUp() }
}

fun View.setupNavTransition() {
    setOnClickListener { TransitionNavigation.navigate(it) }
}

fun View.setupNavTransition(block: () -> Unit) {
    setOnClickListener {
        block()
        TransitionNavigation.navigate(it)
    }
}

fun View.removeSelf() {
    val vg = parent as? ViewGroup
    vg?.removeView(this)
}

fun View.findAll(cond: (View) -> Boolean): MutableList<View> {
    val views = ArrayList<View>()
    if (this is ViewGroup) {
        for (i in 0 until this.childCount) {
            val view = getChildAt(i)
            if (cond.invoke(view)) {
                views.add(view)
            }
        }
    }
    return views
}

fun View.find(cond: (View) -> Boolean): View {
    if (this is ViewGroup) {
        for (i in 0 until this.childCount) {
            val view = getChildAt(i)
            if (cond.invoke(view)) {
                return view
            }
        }
    }
    return this
}
