package com.outofdate.giphy.utils

import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable
import java.util.*

object BundleUtils {

    fun getBundle(vararg args: Any?): Bundle {
        val bundle = Bundle()
        for (i in args.indices) {
            if (i % 2 == 0) {
                put(bundle, args[i].toString(), args[i + 1])
            }
        }
        return bundle
    }

    fun put(bundle: Bundle, key: String, value: Any?) {
        when (value) {
            is Int -> bundle.putInt(key, value)
            is String -> bundle.putString(key, value)
            is Boolean -> bundle.putBoolean(key, value)
            is Parcelable -> bundle.putParcelable(key, value)
            is List<*> -> bundle.putParcelableArrayList(key, value as ArrayList<out Parcelable>)
            is Enum<*> -> bundle.putSerializable(key, value as Serializable)
            is Serializable -> bundle.putSerializable(key, value)
        }
    }
}