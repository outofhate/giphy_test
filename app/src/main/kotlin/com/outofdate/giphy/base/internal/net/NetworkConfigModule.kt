package com.outofdate.giphy.base.internal.net

import android.content.Context
import android.content.pm.PackageManager.GET_META_DATA
import com.outofdate.giphy.utils.Params
import com.outofdate.giphy.utils.Params.Net.apiKey
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkConfigModule {

    @Provides
    @Singleton
    fun baseUrl(): HttpUrl {
        return HttpUrl.parse("https://api.giphy.com")!!
    }

    @Provides
    @Named(apiKey)
    fun apiKey(context: Context): String {
        val info = context.packageManager.getApplicationInfo(context.packageName, GET_META_DATA)
        return info.metaData.getString(Params.Net.giphyApiKey, "")
    }

}
