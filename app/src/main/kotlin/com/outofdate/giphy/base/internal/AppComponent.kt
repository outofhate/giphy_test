package com.outofdate.giphy.base.internal

import android.app.Application
import com.outofdate.giphy.base.internal.android.AndroidModule
import com.outofdate.giphy.base.internal.android.AppModule
import com.outofdate.giphy.base.internal.android.GsonModule
import com.outofdate.giphy.base.internal.kotlin.CoroutineModule
import com.outofdate.giphy.base.internal.net.NetworkConfigModule
import com.outofdate.giphy.base.internal.net.OkHttpModule
import com.outofdate.giphy.base.internal.net.RetrofitModule
import com.outofdate.giphy.modules.GifContainer
import com.outofdate.giphy.modules.GifContainerModule
import dagger.Component
import kotlinx.coroutines.experimental.CoroutineDispatcher
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidModule::class,
    AppModule::class,
    OkHttpModule::class,
    RetrofitModule::class,
    NetworkConfigModule::class,
    GsonModule::class,
    CoroutineModule::class,
    GifContainerModule::class
])
interface AppComponent : DaggerComponent {

    val app: Application

    fun retrofit(): Retrofit
    fun uiCoroutineContext(): CoroutineDispatcher
    fun container(): GifContainer
}