package com.outofdate.giphy.base.mvp

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import android.widget.Toast.makeText
import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.ComponentHolder
import com.outofdate.giphy.base.internal.ComponentsHandler
import com.outofdate.giphy.base.internal.DaggerComponent
import javax.inject.Inject

abstract class MvpActivity<DB : ViewDataBinding, V : Mvp.View, P : Mvp.Presenter<V, out Mvp.Interactor>, C : DaggerComponent> : AppCompatActivity(), Mvp.View, ComponentHolder<C> {
    @Inject
    lateinit var presenter: P

    lateinit var binding: DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector(setupComponent())
        binding = DataBindingUtil.setContentView(this, layout)
        presenter.bind(this as V)
    }

    @get:LayoutRes
    abstract val layout: Int

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbind()
    }

    override fun getAppComponent(): AppComponent {
        return (application as ComponentsHandler).getComponent(AppComponent::class)!!
    }

    override fun setupComponent(): C {
        val components = application as ComponentsHandler
        return if (!components.hasComponent(componentClass)) {
            components.addComponent(getComponent()) as C
        } else components.getComponent(componentClass)!!
    }

    override fun showError(message: String?) {
        if (!isFinishing)
            makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}

