package com.outofdate.giphy.base.android

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Color
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.transition.Fade
import android.view.LayoutInflater
import android.view.ViewGroup
import com.outofdate.giphy.utils.Versions.marshmallow
import com.outofdate.giphy.utils.animation.AnimationsConfig
import com.outofdate.giphy.utils.animation.AnimationsConfig.DURATION
import com.outofdate.giphy.utils.animation.ChangeTransformTransition

abstract class BaseFragment<DB : ViewDataBinding> : Fragment() {

    lateinit var binding: DB
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        marshmallow {
            sharedElementEnterTransition = ChangeTransformTransition(true, DURATION)
            sharedElementReturnTransition = ChangeTransformTransition(false, DURATION - 200)
            if (setupEnterTransitions())
                enterTransition = Fade(Fade.MODE_OUT).setDuration(DURATION).setInterpolator(AnimationsConfig.decelerateInterpolator)
            if (setupExitTransitions())
                exitTransition = Fade(Fade.MODE_IN).setDuration(DURATION).setInterpolator(AnimationsConfig.decelerateInterpolator)
        }
    }

    protected open fun setupExitTransitions(): Boolean {
        return true
    }

    protected open fun setupEnterTransitions(): Boolean {
        return true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): android.view.View? {
        retainInstance = true
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<DB>(inflater, layout, container, false).apply {
            if (root.background == null)
                root.setBackgroundColor(Color.WHITE)
        }
        return binding.root
    }

    @get:LayoutRes
    abstract val layout: Int

    open fun showLoader() = Unit

    open fun hideLoader() = Unit
}