package com.outofdate.giphy.base.internal

class CoroutineCallbackBase<T>(val function: (T?, Throwable?) -> Unit) {
    fun invoke(result: T?, cause: Throwable?)  {
        function.invoke(result, cause)
    }
}