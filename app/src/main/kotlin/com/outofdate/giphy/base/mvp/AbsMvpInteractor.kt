package com.outofdate.giphy.base.mvp

import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.DisposableHandle
import timber.log.Timber

abstract class AbsMvpInteractor(override val main: CoroutineDispatcher) : Mvp.Interactor {

    private val disposables = arrayListOf<DisposableHandle>()

    protected fun Deferred<Any>.disposeOnComplete(c: (cause: Throwable?) -> Unit) = apply { invokeOnCompletion(c::invoke).addToDisposables() }
    protected fun Deferred<Any>.disposeOnComplete() = apply {
        invokeOnCompletion { cause -> Timber.e(cause); }.addToDisposables()
    }

    protected fun DisposableHandle.addToDisposables() {
        if (!disposables.contains(this)) {
            disposables.add(this)
        }
    }

    private fun dispose() {
        disposables.forEach {
            it.dispose()
        }
    }

    override fun disposeAll() {
        dispose()
    }
}
