package com.outofdate.giphy.base.internal.android

import android.app.Application
import com.outofdate.giphy.base.internal.ComponentsHandler
import dagger.Module
import dagger.Provides

@Module
class ComponentsHandlerModule {

    @Provides
    fun componentsHandler(app: Application): ComponentsHandler {
        return app as ComponentsHandler
    }

}