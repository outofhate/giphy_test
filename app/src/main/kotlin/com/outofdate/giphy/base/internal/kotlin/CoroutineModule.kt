package com.outofdate.giphy.base.internal.kotlin

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.android.UI
import javax.inject.Singleton

@Module
class CoroutineModule {

    @Provides
    @Singleton
    fun mainThreadUiDispatcher(): CoroutineDispatcher {
        return UI
    }


}