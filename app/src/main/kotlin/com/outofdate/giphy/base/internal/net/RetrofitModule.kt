package com.outofdate.giphy.base.internal.net

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun retrofit(client: OkHttpClient, url: HttpUrl, gson: GsonConverterFactory): Retrofit {
        val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(gson)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
        return retrofit.build()
    }

}