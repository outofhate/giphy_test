package com.outofdate.giphy.base.internal.android

import android.content.Context
import android.content.SharedPreferences
import com.outofdate.giphy.utils.Params
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AndroidModule {

    @Provides
    @Singleton
    fun getPrefs(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName.plus(Params.Prefs.main), Context.MODE_PRIVATE)
    }

}
