package com.outofdate.giphy.base.internal

import kotlin.reflect.KClass

interface DaggerComponent

interface ComponentHolder<C : DaggerComponent> {
    val componentClass: KClass<C>
    fun getComponent(): C
    fun getAppComponent(): AppComponent
    fun setupComponent(): C
    fun injector(component: C)
}