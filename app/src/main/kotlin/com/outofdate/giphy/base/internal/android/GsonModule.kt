package com.outofdate.giphy.base.internal.android

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class GsonModule {

    @Provides
    @Singleton
    fun gson(): Gson {
        return GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create()
    }

    @Provides
    @Singleton
    fun gsonConverter(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

}