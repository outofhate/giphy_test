package com.outofdate.giphy.base.internal

import kotlin.reflect.KClass

interface ComponentsHandler {
    fun addComponent(component: DaggerComponent?): DaggerComponent?
    fun <T : DaggerComponent> hasComponent(component: KClass<T>): Boolean
    fun <K : DaggerComponent> getComponent(component: KClass<K>): K?
    fun <C : DaggerComponent> removeComponent(component: KClass<C>)
}
