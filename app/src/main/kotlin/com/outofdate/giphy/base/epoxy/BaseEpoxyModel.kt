package com.outofdate.giphy.base.epoxy

import android.content.Context
import android.databinding.ViewDataBinding
import android.support.annotation.StringRes
import com.airbnb.epoxy.DataBindingEpoxyModel
import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.ComponentHolder
import com.outofdate.giphy.base.internal.ComponentsHandler
import com.outofdate.giphy.base.internal.DaggerComponent

abstract class BaseEpoxyModel<DB : ViewDataBinding, DATA, C : DaggerComponent> protected constructor(protected val data: DATA) : DataBindingEpoxyModel(), ComponentHolder<C> {

    protected var binding: DB? = null

    override fun setDataBindingVariables(binding: ViewDataBinding) {
        this.binding = binding as DB
        injector(setupComponent())
        bind(binding)
    }

    protected abstract fun bind(binding: DB)

    protected abstract fun unbind(binding: DB)

    protected fun attached(binding: DB) = Unit

    protected fun detached(binding: DB) = Unit

    override fun unbind(holder: DataBindingEpoxyModel.DataBindingHolder) {
        unbind(holder.dataBinding as DB)
        super.unbind(holder)
        binding = null
    }

    override fun onViewAttachedToWindow(holder: DataBindingEpoxyModel.DataBindingHolder) {
        attached(holder.dataBinding as DB)
        super.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: DataBindingEpoxyModel.DataBindingHolder) {
        detached(holder.dataBinding as DB)
        super.onViewDetachedFromWindow(holder)
    }

    fun hasData(): Boolean {
        return data != null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BaseEpoxyModel<*, *, *>) return false
        if (!super.equals(other)) return false

        if (data != other.data) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun setupComponent(): C {
        val components = componentsHandler()
        return if (!components.hasComponent(componentClass)) {
            components.addComponent(getComponent()) as C
        } else components.getComponent(componentClass)!!
    }

    private fun componentsHandler() = binding?.context()?.applicationContext as ComponentsHandler

    override fun getAppComponent(): AppComponent {
        return componentsHandler().getComponent(AppComponent::class)!!
    }
    protected fun ViewDataBinding.context(): Context = this.root.context
    protected fun ViewDataBinding.getString(@StringRes res: Int): String = context().getString(res)

}