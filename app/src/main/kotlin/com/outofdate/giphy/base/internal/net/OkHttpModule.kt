package com.outofdate.giphy.base.internal.net

import com.outofdate.giphy.utils.Params
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import javax.inject.Named
import javax.inject.Singleton

@Module
class OkHttpModule {

    @Provides
    @Singleton
    fun client(interceptor: Interceptor): OkHttpClient {
        val builder = OkHttpClient
                .Builder()
                .addInterceptor(
                        HttpLoggingInterceptor {
                            Timber.d(it)
                        }
                                .setLevel(HttpLoggingInterceptor.Level.BASIC)
                )
                .addInterceptor(interceptor)
        return builder.build()
    }

    @Provides
    @Singleton
    fun interceptor(@Named(Params.Net.apiKey) apiKey: String): Interceptor {
        return BaseInterceptor(apiKey)
    }

    class BaseInterceptor(private val apiKey: String) : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            return chain.proceed(chain.request().newBuilder()
                    .addHeader(Params.Net.apiKey, apiKey)
                    .build())
        }
    }
}