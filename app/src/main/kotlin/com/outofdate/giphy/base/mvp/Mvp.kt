package com.outofdate.giphy.base.mvp

import kotlinx.coroutines.experimental.CoroutineDispatcher

interface Mvp {

    interface View {
        fun showLoader()
        fun hideLoader()
        fun showError(message: String?)
    }

    interface Presenter<V : View, I : Interactor> {
        fun bind(view: V) = Unit
        fun unbind() = Unit
    }

    interface Interactor {
        fun disposeAll()
        val main: CoroutineDispatcher
    }

}