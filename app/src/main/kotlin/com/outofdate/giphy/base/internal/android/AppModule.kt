package com.outofdate.giphy.base.internal.android

import android.app.Application
import android.content.Context
import com.outofdate.giphy.Giphy
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: Application) {

    @Provides
    fun app(): Application {
        return application
    }

    @Provides
    fun giphy(): Giphy {
        return application as Giphy
    }

    @Provides
    fun appContext(): Context {
        return application
    }

}