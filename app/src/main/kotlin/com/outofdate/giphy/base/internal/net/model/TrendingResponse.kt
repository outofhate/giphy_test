package com.outofdate.giphy.base.internal.net.model

import com.google.gson.annotations.SerializedName
import com.outofdate.giphy.base.internal.net.model.TrendingResponse.Data
import com.outofdate.giphy.base.internal.net.model.TrendingResponse.Meta

data class DataResponse(@SerializedName("data") val data: Data, @SerializedName("meta") val meta: Meta)

data class TrendingResponse(
        @SerializedName("data") val data: List<Data>,
        @SerializedName("pagination") val pagination: Pagination,
        @SerializedName("meta") val meta: Meta
) {

    data class Pagination(
            @SerializedName("total_count") val totalCount: Int, // 58575
            @SerializedName("count") val count: Int, // 25
            @SerializedName("offset") val offset: Int // 0
    )


    data class Meta(
            @SerializedName("status") val status: Int, // 200
            @SerializedName("msg") val msg: String, // OK
            @SerializedName("response_id") val responseId: String // 5b8bb04f4144614a4ddb3363
    )


    data class Data(
            @SerializedName("type") val type: String, // gif
            @SerializedName("id") val id: String, // l3V0Akmv7LTfIHK6c
            @SerializedName("slug") val slug: String, // planckendael-l3V0Akmv7LTfIHK6c
            @SerializedName("url") val url: String, // https://giphy.com/gifs/planckendael-l3V0Akmv7LTfIHK6c
            @SerializedName("bitly_gif_url") val bitlyGifUrl: String, // https://gph.is/1SpVvtz
            @SerializedName("bitly_url") val bitlyUrl: String, // https://gph.is/1SpVvtz
            @SerializedName("embed_url") val embedUrl: String, // https://giphy.com/embed/l3V0Akmv7LTfIHK6c
            @SerializedName("username") val username: String, // planckendael
            @SerializedName("source") val source: String, // www.planckendael.be
            @SerializedName("rating") val rating: String, // g
            @SerializedName("content_url") val contentUrl: String,
            @SerializedName("source_tld") val sourceTld: String,
            @SerializedName("source_post_url") val sourcePostUrl: String, // www.planckendael.be
            @SerializedName("is_sticker") val isSticker: Int, // 0
            @SerializedName("import_datetime") val importDatetime: String, // 2016-04-19 12:15:30
            @SerializedName("trending_datetime") val trendingDatetime: String, // 2018-09-02 03:30:01
            @SerializedName("user") val user: User?,
            @SerializedName("images") val images: Images,
            @SerializedName("title") val title: String, // hugging hug GIF by Planckendael
            @SerializedName("_score") val score: Int // 0
    ) {

        data class Images(
                @SerializedName("fixed_height_still") val fixedHeightStill: ImageDescription,
                @SerializedName("original_still") val originalStill: ImageDescription,
                @SerializedName("fixed_width") val fixedWidth: ImageDescription,
                @SerializedName("fixed_height_small_still") val fixedHeightSmallStill: ImageDescription,
                @SerializedName("fixed_height_downsampled") val fixedHeightDownsampled: ImageDescription,
                @SerializedName("preview") val preview: ImageDescription,
                @SerializedName("fixed_height_small") val fixedHeightSmall: ImageDescription,
                @SerializedName("downsized_still") val downsizedStill: ImageDescription,
                @SerializedName("downsized") val downsized: ImageDescription,
                @SerializedName("downsized_large") val downsizedLarge: ImageDescription,
                @SerializedName("fixed_width_small_still") val fixedWidthSmallStill: ImageDescription,
                @SerializedName("preview_webp") val previewWebp: ImageDescription,
                @SerializedName("fixed_width_still") val fixedWidthStill: ImageDescription,
                @SerializedName("fixed_width_small") val fixedWidthSmall: ImageDescription,
                @SerializedName("downsized_small") val downsizedSmall: ImageDescription,
                @SerializedName("fixed_width_downsampled") val fixedWidthDownsampled: ImageDescription,
                @SerializedName("downsized_medium") val downsizedMedium: ImageDescription,
                @SerializedName("original") val original: ImageDescription,
                @SerializedName("fixed_height") val fixedHeight: ImageDescription,
                @SerializedName("looping") val looping: ImageDescription,
                @SerializedName("original_mp4") val originalMp4: ImageDescription,
                @SerializedName("preview_gif") val previewGif: ImageDescription,
                @SerializedName("480w_still") val previewStill: ImageDescription
        ) {

            data class ImageDescription(
                    @SerializedName("url") val url: String, // https://media3.giphy.com/media/l3V0Akmv7LTfIHK6c/200.gif
                    @SerializedName("width") val width: Int, // 268
                    @SerializedName("height") val height: Int, // 200
                    @SerializedName("size") val size: Long, // 3375118
                    @SerializedName("frames") val frames: Int, // 108
                    @SerializedName("mp4") val mp4: String, // https://media3.giphy.com/media/l3V0Akmv7LTfIHK6c/200.mp4
                    @SerializedName("mp4_size") val mp4Size: Long, // 106590
                    @SerializedName("webp") val webp: String, // https://media3.giphy.com/media/l3V0Akmv7LTfIHK6c/200.webp
                    @SerializedName("webp_size") val webpSize: Long// 1089572
            )
        }


        data class User(
                @SerializedName("avatar_url") val avatarUrl: String, // https://media4.giphy.com/avatars/planckendael/YMh8dgse55lC.jpg
                @SerializedName("banner_url") val bannerUrl: String, // https://media4.giphy.com/headers/planckendael/i570VdMBqz18.png
                @SerializedName("banner_image") val bannerImage: String, // https://media4.giphy.com/headers/planckendael/i570VdMBqz18.png
                @SerializedName("profile_url") val profileUrl: String, // https://giphy.com/planckendael/
                @SerializedName("username") val username: String, // planckendael
                @SerializedName("display_name") val displayName: String, // Planckendael
                @SerializedName("is_verified") val isVerified: Boolean // false
        )
    }
}