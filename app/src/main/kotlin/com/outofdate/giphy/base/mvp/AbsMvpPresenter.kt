package com.outofdate.giphy.base.mvp

import kotlinx.coroutines.experimental.launch

abstract class AbsMvpPresenter<V : Mvp.View, I : Mvp.Interactor>(protected var interactor: I) : Mvp.Presenter<V, I> {

    protected var view: V? = null

    override fun bind(view: V) {
        super.bind(view)
        this.view = view
    }

    override fun unbind() {
        super.unbind()
        interactor.disposeAll()
        this.view = null
    }

    open fun V.ui(block: V.() -> Unit) {
        launch(interactor.main) {
            view(block)
        }
    }

    open fun Mvp.Presenter<*,*>.view(block: V.() -> Unit) {
        view?.block()
    }
}
