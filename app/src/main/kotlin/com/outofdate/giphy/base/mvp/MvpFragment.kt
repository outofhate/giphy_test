package com.outofdate.giphy.base.mvp

import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import com.outofdate.giphy.base.android.BaseFragment
import com.outofdate.giphy.base.internal.AppComponent
import com.outofdate.giphy.base.internal.ComponentHolder
import com.outofdate.giphy.base.internal.ComponentsHandler
import com.outofdate.giphy.base.internal.DaggerComponent
import javax.inject.Inject

abstract class MvpFragment<DB : ViewDataBinding, V : Mvp.View, P : Mvp.Presenter<V, out Mvp.Interactor>, C : DaggerComponent> : BaseFragment<DB>(), Mvp.View, ComponentHolder<C> {
    @Inject
    lateinit var presenter: P

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        injector(setupComponent())
        val view = super.onCreateView(inflater, container, savedInstanceState)
        presenter.bind(this as V)
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unbind()
    }

    override fun getAppComponent(): AppComponent {
        return (activity?.application as ComponentsHandler).getComponent(AppComponent::class)!!
    }

    override fun setupComponent(): C {
        val components = activity?.application as ComponentsHandler
        return if (!components.hasComponent(componentClass)) {
            components.addComponent(getComponent()) as C
        } else components.getComponent(componentClass)!!
    }

    override fun showError(message: String?) {
        if (context != null)
            makeText(context, message ?: "No message", LENGTH_SHORT).show()
    }

}

