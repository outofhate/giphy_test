package com.outofdate.giphy.base.internal.net.servicemodules

import com.outofdate.giphy.base.internal.net.services.GiphyGifsService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class GifApiModule {

    @Provides
    fun gifService(retrofit: Retrofit): GiphyGifsService {
        return retrofit.create(GiphyGifsService::class.java)
    }

}