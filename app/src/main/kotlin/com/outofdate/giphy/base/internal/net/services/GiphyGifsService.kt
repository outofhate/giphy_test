package com.outofdate.giphy.base.internal.net.services

import com.outofdate.giphy.base.internal.net.model.DataResponse
import com.outofdate.giphy.base.internal.net.model.TrendingResponse
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GiphyGifsService {

    @GET("v1/gifs/trending")
    fun trending(
            @Query("limit") limit: Int,
            @Query("rating") rating: String?,
            @Query("offset") offset: Int
    ): Deferred<TrendingResponse?>

    @GET("v1/gifs/{id}")
    fun byId(@Path("id") id: String?): Deferred<DataResponse?>
}