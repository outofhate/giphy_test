package com.outofdate.giphy

import timber.log.Timber

class GiphyDebug : Giphy() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}


