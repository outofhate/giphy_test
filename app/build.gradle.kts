import com.android.build.gradle.ProguardFiles.getDefaultProguardFile
import org.apache.commons.logging.LogFactory.release
import org.gradle.internal.impldep.bsh.commands.dir
import org.gradle.internal.impldep.com.amazonaws.PredefinedClientConfigurations.defaultConfig
import org.jetbrains.kotlin.config.AnalysisFlag.Flags.experimental
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

val kotlin_version: String by parent!!.extra

buildscript {
    configure(listOf(repositories, project.repositories)) {
        gradlePluginPortal()
        google()
    }
}
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
}
apply {
    plugin("kotlin-android")
}
kotlin.experimental.coroutines = Coroutines.ENABLE

android {
    dataBinding.isEnabled = true
    buildToolsVersion("28.0.2")
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "com.outofdate.giphy"
        minSdkVersion(16)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            versionNameSuffix = "-SNAPSHOT"
        }
    }
    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("debug").java.srcDirs("src/debug/kotlin")
    }
}

dependencies {

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    testImplementation("junit:junit:4.12")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
    androidTestImplementation("com.android.support.test:runner:1.0.2")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
    implementation("com.android.support:appcompat-v7:28.0.0-rc01")
    implementation("com.android.support:recyclerview-v7:28.0.0-rc01")
    implementation("com.android.support:support-core-utils:28.0.0-rc01")
    implementation("com.android.support:design:28.0.0-rc01")
    implementation("com.squareup.retrofit2:retrofit:2.4.0")
    implementation("com.squareup.okhttp3:logging-interceptor:3.10.0")
    implementation("com.google.code.gson:gson:2.8.5")
    implementation("com.squareup.retrofit2:converter-gson:2.4.0")
    implementation("com.airbnb.android:epoxy:2.14.0")
    implementation("com.airbnb.android:epoxy-databinding:2.14.0")
    implementation("com.jakewharton.timber:timber:4.7.1")
    implementation("com.jakewharton.retrofit:retrofit2-kotlin-coroutines-experimental-adapter:1.0.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:0.25.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:0.25.0")
    implementation("com.google.dagger:dagger:2.17")

    implementation("com.google.code.findbugs:jsr305:3.0.2")

    kapt("com.google.dagger:dagger-compiler:2.17")

    implementation(kotlin("reflect", "1.2.61"))

    implementation("com.android.support.constraint:constraint-layout:2.0.0-alpha2")

    implementation("android.arch.navigation:navigation-fragment:1.0.0-alpha05")
    implementation("android.arch.navigation:navigation-ui:1.0.0-alpha05")

    //Fresco image library
    implementation("com.facebook.fresco:fresco:1.10.0")
    implementation("com.facebook.fresco:animated-gif:1.10.0")
    implementation("com.facebook.fresco:webpsupport:1.10.0")
    implementation("com.facebook.fresco:animated-webp:1.10.0")
}
repositories {
    mavenCentral()
}