rootProject.buildFileName = "build.gradle.kts"
pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "com.android.application") {
                println("${requested.version} ${requested.id.id}")
                useModule("com.android.tools.build:gradle:${requested.version}")
            }
        }
    }
}
include(":app")
